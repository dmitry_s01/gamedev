﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameDev
{
    public interface ArtefactInterface
    {
        int power(); // мощность артефакта
        bool renewable(); // признак возобновляемости
    }
}
