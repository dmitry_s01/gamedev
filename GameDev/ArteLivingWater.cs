﻿using System;
using System.Collections.Generic;
using System.Text;


namespace GameDev
{
    public enum ArteLivingWaterVolume
    {
        small = 10,
        medium = 25,
        large = 50
    }

    //живая вода
    public class ArteLivingWater : ArtefactInterface
    {
        ArteLivingWaterVolume m_volume;
        public ArteLivingWater(ArteLivingWaterVolume volume)
        {
            m_volume = volume;
        }

        public virtual bool renewable() { return false; }
        public virtual int power() { return 1; }
    }
}
