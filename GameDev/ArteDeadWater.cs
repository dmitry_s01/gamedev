﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameDev
{
    public enum ArteDeadWaterVolume
    {
        small = 10,
        medium = 25,
        large = 50
    }

    //живая вода
    public class ArteDeadWater : ArtefactInterface
    {
        ArteDeadWaterVolume m_volume;

        public ArteDeadWater(ArteDeadWaterVolume volume)
        {
            m_volume = volume;
        }

        public virtual bool renewable() { return false; }
        public virtual int power() { return 2; }
    }
}
