﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Game___Part_1__
{
    enum Gender
    {
        Male,
        Female
    }

    enum Race
    {
        Human,
        Gnome,
        Elf,
        Orc,
        Goblin
    }

    enum Condition
    {
        Normal,
        Weakened,
        Ill,
        Poisoned,
        Paralyzed,
        Dead
    }
    
    class PersonRP : IComparable <PersonRP>
    {
        static int ID = 1;
        private int personal_ID;
        private Gender gender_of_person; 
        private Race race_of_person; 
        public Condition condition_of_person; 
        private string name_of_person; 
        public uint age_of_person; 
        public float current_amount_of_hp; 
        public uint current_exp; 
        public float max_hp;

        public PersonRP(Gender value_gop, Race value_rof, Condition value_cop, String value_nop, uint value_aop, uint value_caoh, uint value_ce, uint value_mh) 
        {
            gender_of_person = value_gop;
            race_of_person = value_rof;
            Set_Condition(value_cop);
            Set_Nameofperson(value_nop);
            Set_Ageofperson(value_aop);
            Set_Currentamountofhp(value_caoh);
            Set_Currentexp(value_ce);
            Set_Maxhp(value_mh);
            personal_ID = ID;
            ID++;
        }

        ~PersonRP() { }

        public Gender Get_Gender()
        {
            return gender_of_person;
        }

        public Race Get_Race()
        {
            return race_of_person;
        }

        public Condition Get_Condition()
        {
            return condition_of_person;
        }

        public void Set_Condition(Condition value)
        {
            condition_of_person = value;
        }

        public void Set_Nameofperson(string value)
        {
            name_of_person = value;
        }

        public string Get_Nameofperson()
        {
            return name_of_person;
        }

        public void Set_Ageofperson(uint value)
        {
            age_of_person = value;
        }

        public uint Get_Ageofperson()
        {
            return age_of_person;
        }

        public void Set_Currentamountofhp (float value)
        {
            if (value < 0)
            {
                throw new Exception("Текущее состояние здоровья не может быть меньше нуля!");
            }
            else {
                current_amount_of_hp = value;
            }
            if (value == 0)
            {
                this.Set_Condition(Condition.Dead);
            }
            else if ((value / max_hp * 100) < 10)
            {
                this.Set_Condition(Condition.Weakened);
            }
            else if ((value / max_hp * 100) >= 10)
            {
                this.Set_Condition(Condition.Normal);
            }
        }

        public float Get_Currentamountofhp()
        {
            return current_amount_of_hp;
        }

        public void Set_Currentexp (uint value)
        {
            current_exp = value;
        }

        public uint Get_Currentexp()
        {
            return current_exp;
        }

        public void Set_Maxhp(uint value)
        {
            if (value <= 0)
            {
                throw new Exception("Максимальное здоровье не может быть меньше или равняться нулю!");
            }
            else
            {
                max_hp = value;
            }
        }

        public float Get_Maxhp()
        {
            return max_hp;
        }

        public int Get_ID()
        {
            return personal_ID;
        }

        public void Speak(string value)
        {
            Console.WriteLine(this.Get_Nameofperson() + " " + "говорит: " + value);
        }

        public void Move (bool value)
        {
            if(value == true)
            {
                Console.WriteLine(this.Get_Nameofperson() + " " + "двигается.");
            }
            else if (value == false)
            {
                Console.WriteLine(this.Get_Nameofperson() + " " + "стоит на месте.");
            }
        }

        public void InfoCharacter()
        {
            Console.WriteLine("Имя персонажа: " + " " + (this.name_of_person).ToString());
            Console.WriteLine("ID:" + " " + (this.personal_ID).ToString());
            Console.WriteLine("Пол:" + " " + (this.gender_of_person).ToString());
            Console.WriteLine("Раса:" + " " + (this.race_of_person).ToString());
            Console.WriteLine("Состояние:" + " " + (this.condition_of_person).ToString());
            Console.WriteLine("Возраст:" + " " + (this.age_of_person).ToString());
            Console.WriteLine("Максимальное здоровье:" + " " + (this.max_hp).ToString());
            Console.WriteLine("Текущее здоровье:" + " " + (this.current_amount_of_hp).ToString());
            Console.WriteLine("Текущий опыт:" + " " + (this.current_exp).ToString());
        }

        public int CompareTo(PersonRP value)
        {
            return this.current_amount_of_hp.CompareTo(value.current_amount_of_hp);
        }
        










    }
}
