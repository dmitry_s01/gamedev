﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameDev
{
    public class ArtePoisonousSaliva : ArtefactInterface
    {
        public virtual bool renewable() { return true; }
        public virtual int power() { return 2; }
    }
}
