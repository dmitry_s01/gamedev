﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameDev
{
    public class ArteVasiliskEye
    {
        public virtual bool renewable() { return false; }
        public virtual int power() { return 2; }
    }
}
